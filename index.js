class Animal {
    constructor(whiskers, sounds, disposition = 'passive') {
        this.whiskers = whiskers
        this.disposition = disposition
        this.sounds = sounds
    }

    speak() {
        console.log(this.sounds[this.disposition] || this.sounds['default'])
    }

    countWhiskers() {
        for (let counter = 0; counter < this.whiskers; counter++) {
            this.speak()
        }
    }

    eat(food = 'food') {
        if (food === 'catnip') {
            this.disposition = this instanceof Cat ? 'high' : 'confused'
            return
        }

        if (this.disposition === 'angry') {
            this.disposition = 'passive'
        } else {
            this.disposition = 'happy'
        }
    }
}

class Cat extends Animal {
    constructor(whiskers, disposition = 'passive') {
        super(whiskers, {
            angry: "hiss",
            happy: "purr",
            high: "*stares at you in cat*",
            default: "meow"
        }, disposition)
    }
}

class Dog extends Animal {
    constructor(whiskers, disposition = 'passive') {
        super(whiskers, {
            angry: "grrr!",
            happy: "bork bork bork!",
            confused: "y u do me a bamboozle fren?",
            default: "bork"
        }, disposition)
    }
}

const cat = new Cat(8, 'angry')
cat.eat()
cat.eat('catnip')
cat.countWhiskers()

const dog = new Dog(12, 'angry')
dog.eat('catnip')
dog.speak()
import React from 'react'
import styled from 'styled-components'
import CatsPage from './Pages/Cats'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  NavLink as Link
} from "react-router-dom"

const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background: rgb(240, 242, 245);
`

const NavBar = styled.nav`
  box-sizing: border-box;
  width: 100%;
  padding: 1em;
  background: #001529; 
  height: 3em;
  display: flex;
  color: white;

  & a {
    text-decoration: none;
    color: white;
    height: 3em;
    display: block;
    margin-top: -1em;
    line-height: 3em;
    padding: 0 1em;
  }

  & a.active {
    background: #1890ff;
  }
`

const Title = styled.div`

`

const List = styled.div`
  display: flex;
  margin: 0 2em;
`

const ListItem = styled.div`

`

const Content = styled.div`
  background: white;
  width: 100%;
  height: calc(100% - 3em);
  padding: 1em;
  box-sizing: border-box;
  z-index: 100;

  @media screen and (min-width: 800px) {
    border-radius: 2px;
    width: 92vw;
    height: calc(100vh - 1.375em - 8em);
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -47%);
    padding: 3em;    
  }
`

const LightBox = styled.div`
  display: block;
  pointer-events: none;
  transition: opacity .5s;
  background: rgba(0,0,0,.75);
  opacity: 0;
  width: 100vw;
  height: 100vh;
  position: fixed;
  left: 0;
  top: 0;
  
  ${props => props.open ? `
    opacity: 1;
    pointer-events: auto;
  ` : ''}

  @media screen and (max-width: 800px) {
    height: 3em;
  }
`

function App() {
  const [lightBoxIsOpen, setLightBoxIsOpen] = React.useState(false)

  return (
    <Container>
      <Router>
        <NavBar>
          <Link to="/" activeClassName="active" exact>
            <Title>AppName</Title>
          </Link>
          <List>
            <ListItem>
              <Link to="/cats" activeClassName="active">Cats</Link>
            </ListItem>
          </List>
        </NavBar>

        <Content>
          <Switch>
            <Route path="/" exact>
              Dashboard
            </Route>
            <Route path="/cats">
              <CatsPage setLightBoxIsOpen={setLightBoxIsOpen} />
            </Route>
          </Switch>
        </Content>
      </Router>

      <LightBox open={lightBoxIsOpen} />
    </Container>
  )
}

export default App
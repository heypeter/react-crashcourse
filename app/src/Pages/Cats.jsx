import React from 'react'
import styled from 'styled-components'
import FAB from './Components/FAB'
import Cat from '../Models/Cat'

const Container = styled.div`
    width: 100%;
    height: 100%;
`

const Form = styled.div`

`

const Field = styled.div`
    display: flex;
    flex-direction: column;
`

const Input = styled.input`
    border: 1px solid rgba(0,0,0,.1);
    border-radius: 4px;
    padding: .5em;

    &::placeholder {
        color: #bdbdbd;
    }

    &:focus {
        outline: none;
        border: 1px solid #1890ff;
    }

    &.error {
        border: 1px solid red;
    }
`

const Label = styled.label`
    font-weight: 100;
`

const Heading = styled.h1`
    font-weight: 100;
    margin-top: .25em;
`

const Cancel = styled.div`
    &:before {
        content: 'Cancel';
        position: absolute;
        bottom: 2.5em;
        right: 6.25em;
        cursor: pointer;
        color: #f5222d;
    }
`

const Table = styled.div`
    display: flex;
    flex-direction: column;
`

const Row = styled.div`
    display: flex;
    border-bottom: 1px solid rgba(0,0,0,.1);    

    &:hover {
        background: #efefef;
    }
`

const Col = styled.div`
    padding: 1em 1em;
    flex-grow: 1;
    text-align: left;
`

const Actions = styled(Col)`
    text-align: right;
    flex-grow: 0;
`


const IconButton = styled.div`
    cursor: pointer;
    display: inline-block;
    padding: .25em;

    &:before {
        content: "${props => props.icon}";
    }
`

class CatsPage extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            creatingCat: false,
            name: "",
            cats: [],
            editingCat: false
        }

        this.handleFABClick = this.handleFABClick.bind(this)
        this.handleCancelClick = this.handleCancelClick.bind(this)
        this.handleNameChange = this.handleNameChange.bind(this)
        this.handleDeleteCatClick = this.handleDeleteCatClick.bind(this)
        this.handleEditCatClick = this.handleEditCatClick.bind(this)
    }

    componentDidMount() {
        const catData = JSON.parse(window.localStorage.getItem('cats'))

        catData && this.setState({ cats: catData.map(data => new Cat(data)) })
    }

    handleFABClick(state, event) {
        if (state === 'add') {
            this.props.setLightBoxIsOpen(true)
            this.setState({
                creatingCat: true
            })
            setTimeout(() => this.nameInput.focus(), 100)
        } else {
            if (this.validateForm()) {
                this.props.setLightBoxIsOpen(false)
                if (this.state.creatingCat) {
                    this.createCat(this.state.name)
                } else if (this.state.editingCat) {
                    const cat = this.state.cats.find(cat => cat.id === this.state.selectedCatId)
                    cat.setName(this.state.name)
                    this.editCat(cat)
                }

                this.setState({
                    creatingCat: false,
                    editingCat: false,
                    name: ""
                })
            }

        }
    }

    handleCancelClick() {
        this.props.setLightBoxIsOpen(false)
        this.setState({
            creatingCat: false,
            editingCat: false,
            name: "",
            selectedCatId: null
        })
        if (this.nameInput.classList.contains('error')) {
            this.nameInput.classList.remove('error')
        }
    }

    handleNameChange(event) {
        const name = event.target.value
        this.setState({
            name
        })
        if (name && this.nameInput.classList.contains('error')) {
            this.nameInput.classList.remove('error')
        }
    }

    handleDeleteCatClick(event) {
        const id = event.target.dataset.catId
        this.deleteCat(id)
    }

    handleEditCatClick(event) {
        this.props.setLightBoxIsOpen(true)
        const id = event.target.dataset.catId
        const cat = this.state.cats.find(cat => cat.id === id)
        this.setState({
            editingCat: true,
            name: cat.getName(),
            selectedCatId: cat.getId()
        })
        setTimeout(() => this.nameInput.focus(), 100)
    }

    validateForm() {
        if (this.state.name === "") {
            this.nameInput.classList.add('error')
            return false;
        }

        return true;
    }

    createCat(name) {
        const cat = new Cat()
        cat.setName(name)
        const cats = [...this.state.cats, ...[cat]]
        window.localStorage.setItem('cats', JSON.stringify(cats))
        console.log('setting cats', window.localStorage.getItem('cats'))
        this.setState({
            cats: cats
        })
    }

    deleteCat(id) {
        const cats = this.state.cats.filter(cat => cat.id !== id)
        window.localStorage.setItem('cats', JSON.stringify(cats))
        this.setState({
            cats
        })
    }

    editCat(cat) {
        const cats = [
            ...this.state.cats.filter(stateCat => stateCat.getId() !== cat.getId()),
            ...[cat]
        ]
        window.localStorage.setItem('cats', JSON.stringify(cats))

        this.setState({
            cats
        })
    }

    render() {
        const {
            selectedCatId,
            cats,
            creatingCat,
            editingCat
        } = this.state

        const selectedCat = selectedCatId ? cats.find(cat => cat.id === selectedCatId) : null

        return <Container>
            {creatingCat || (editingCat && selectedCat) ?
                <Form>
                    <Heading>{creatingCat ? 'Create a Cat' : `Edit ${selectedCat.getName()}`}</Heading>
                    <Field>
                        <Label>Name</Label>
                        <Input
                            placeholder="Squiggles McGiggles"
                            ref={node => this.nameInput = node}
                            value={this.state.name}
                            onChange={this.handleNameChange}
                        />
                    </Field>
                </Form>
                : null}

            {!this.state.creatingCat && !this.state.editingCat ?
                <Table>
                    {this.state.cats.map(cat => <Row>
                        <Col>{cat.getName()}</Col>
                        <Actions>
                            <IconButton
                                data-cat-id={cat.getId()}
                                onClick={this.handleEditCatClick}
                            >✎</IconButton>

                            <IconButton
                                data-cat-id={cat.getId()}
                                onClick={this.handleDeleteCatClick}
                                style={{ color: 'red' }}
                            >✖</IconButton>
                        </Actions>
                    </Row>)}
                </Table>
                : null}

            {this.state.creatingCat || this.state.editingCat ? <Cancel onClick={this.handleCancelClick} /> : null}
            <FAB
                onClick={this.handleFABClick}
                state={this.state.creatingCat || this.state.editingCat ? 'confirm' : 'add'}
            >+</FAB>
        </Container>
    }
}

export default CatsPage
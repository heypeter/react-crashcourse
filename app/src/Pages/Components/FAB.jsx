import React from 'react'
import styled from 'styled-components'

const fabDropShadow = '0 0 2px 4px rgba(0,0,0,.1)'

const FAB = styled.button`
    width: 4rem;
    height: 4rem;
    border-radius: 50%;
    background: #40a9ff;
    position: absolute;
    bottom: 1rem;
    right: 1rem;
    color: white;
    font-size: 2em;
    border: 0;
    cursor: pointer;
    user-select: none;
    outline: none;
    transition: box-shadow .5s, background .5s, transform .25s;

    &:before {
        display: block;
        content: "|";
        width: 100%;
        height: 100%;
        position: relative;
        top: 0;
        text-align: center;
        line-height: 54px;
        transition: transform .25s;
        transform: ${props => props.state === 'confirm' ?
        `rotate(138deg) scaleX(1) scaleY(.5) translate(11px, 3px)`
        : `rotate(90deg) scale(.66)`
    };
        
    }

    &:after {
        display: block;
        content: "|";
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        line-height: 59px;
        transition: transform .25s;
        transform: ${props => props.state === 'confirm' ?
        `scale(1) rotate(45deg) translate(5px, -5px)`
        : `scale(.66)`
    };
    }

    &:hover {
        box-shadow: ${fabDropShadow}, inset 0 0 4px rgba(255,255,255,.75);
    }

    &:active {
        transition: box-shadow 0s;
        box-shadow: ${fabDropShadow}, inset 0 0 10px rgba(0,0,0,.75);       
    }

    ${props => props.state === 'cancel' ? `
        background: #f5222d;
        transform: rotate(45deg);
    ` : ''}

    ${props => props.state === 'confirm' ? `
        background: #52c41a;
    ` : ''}
`

export default (props) => <FAB onClick={(event) => { props.onClick(props.state, event) }} state={props.state} />
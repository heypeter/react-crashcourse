class CatApi {
    constructor() {
        this.counter = 200
    }

    async getRandomPicture() {
        const counter = this.counter
        const response = await fetch(`http://placekitten.com/g/${counter}/${counter}`)
        this.counter += 1
        const blob = await response.blob()


        const data = new Promise(resolve => {
            const reader = new FileReader()
            reader.onload = function () {
                resolve(this.result)
            }
            reader.readAsDataURL(blob)
        })

        return await data
    }
}

export default CatApi
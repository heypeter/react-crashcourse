import uuidv4 from 'uuid/v4'

export default class Cat {
    constructor(data) {
        if (data) {
            if ((!data.id || !data.name)) {
                throw new Error("Cat data validation error")
            }

            this.id = data.id
            this.name = data.name
            this.image = data.image
        } else {
            this.id = uuidv4()
        }
    }

    getId() {
        return this.id;
    }

    getName() {
        return this.name
    }

    setName(name) {
        this.name = name
        return this;
    }

    getImage() {
        return this.image
    }

    setImage(image) {
        this.image = image
        return this
    }
}